dcup:
	docker-compose down && docker-compose up --build

dclog:
	docker logs -f test_task_backend

dcdown:
	docker-compose down

dcsh:
	docker exec -it test_task_backend sh