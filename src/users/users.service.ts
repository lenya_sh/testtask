import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { MainException } from '../exceptions/main.exception';
import { UserEntity } from './entities/user.entity';
import { CreateUserRequest, CreateUserResponse } from './dto/createUser.dto';
import {
  GetUserResponse,
  GetUsersRequest,
  GetUsersResponse,
} from './dto/getUser.dto';
import { UpdateUserRequest, UpdateUserResponse } from './dto/updateUser.dto';
import { DeleteUserResponse } from './dto/deleteUser.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  public async createUser(
    request: CreateUserRequest,
  ): Promise<CreateUserResponse> {
    if (await this.isEmailAlreadyExist(request.email))
      throw MainException.invalidData('User with this email already exist');

    const newUser = this.userRepository.create({
      email: request.email,
      password: await bcrypt.hash(request.password, await bcrypt.genSalt(10)),
    });

    return new CreateUserResponse(await this.userRepository.save(newUser));
  }

  public async getUsers(request: GetUsersRequest): Promise<GetUsersResponse> {
    const page = request.page || 1;
    const limit = request.limit || 10;
    const skip = (page - 1) * limit;

    const [users, count] = await this.userRepository.findAndCount({
      take: limit,
      skip: skip,
    });

    return new GetUsersResponse(users, count);
  }

  public async getUserById(userId: number): Promise<GetUserResponse> {
    const user = await this.userRepository.findOne({
      where: {
        id: userId,
      },
    });

    if (!user)
      throw MainException.invalidData(`User with id ${userId} not found`);

    return new GetUserResponse(user);
  }

  public async getUserByEmail(email: string): Promise<UserEntity | undefined> {
    return await this.userRepository.findOne({
      where: {
        email: email,
      },
    });
  }

  public async updateUser(
    request: UpdateUserRequest,
  ): Promise<UpdateUserResponse> {
    const { user } = await this.getUserById(request.userId);

    if (request.email) {
      if (await this.isEmailAlreadyExist(request.email))
        throw MainException.conflict('User with this email already exist');

      user.email = request.email;
    }

    if (request.password)
      user.password = await bcrypt.hash(
        request.password,
        await bcrypt.genSalt(10),
      );

    return new UpdateUserResponse(await this.userRepository.save(user));
  }

  public async deleteUserById(userId: number): Promise<DeleteUserResponse> {
    const deletedResult = await this.userRepository.softDelete(userId);
    if (deletedResult.affected < 1)
      throw MainException.invalidData(
        `Failed to remove user with id ${userId}`,
      );

    return new DeleteUserResponse(true);
  }

  private async isEmailAlreadyExist(email: string): Promise<boolean> {
    return this.userRepository.exist({
      where: {
        email: email,
      },
    });
  }
}
