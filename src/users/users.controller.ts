import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UsePipes,
  ValidationPipe,
  UseFilters,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserRequest } from './dto/createUser.dto';
import { GetUsersRequest } from './dto/getUser.dto';
import { UpdateUserRequest } from './dto/updateUser.dto';
import { MainExceptionFilter } from '../exceptions/main-exception.filter';

@Controller('users')
@UseFilters(MainExceptionFilter)
@UsePipes(ValidationPipe)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() request: CreateUserRequest) {
    return this.usersService.createUser(request);
  }

  @Get()
  getAll(@Query() query: GetUsersRequest) {
    return this.usersService.getUsers(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.getUserById(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: UpdateUserRequest) {
    const request = new UpdateUserRequest(+id, body.email, body.password);
    return this.usersService.updateUser(request);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.deleteUserById(+id);
  }
}
