import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { NewsEntity } from '../../news/entities/news.entity';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ name: 'email', type: 'varchar', length: 512, unique: true })
  public email!: string;

  @Column({ name: 'password', type: 'varchar', length: 128 })
  public password!: string;

  @OneToMany(() => NewsEntity, (news) => news.owner)
  public news!: NewsEntity[];

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  public createdAt!: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  public updatedAt!: Date;

  @DeleteDateColumn({ name: 'deleted_at', type: 'timestamp' })
  public deletedAt!: Date;
}
