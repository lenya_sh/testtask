import {
  IsDefined,
  IsEmail,
  IsStrongPassword,
  MinLength,
} from 'class-validator';
import { UserEntity } from '../entities/user.entity';

export class CreateUserRequest {
  @IsDefined({ message: 'password is defined' })
  @IsEmail()
  public email!: string;

  @IsDefined({ message: 'password is defined' })
  @MinLength(8)
  @IsStrongPassword()
  public password!: string;

  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}

export class CreateUserResponse {
  public user: UserEntity;

  constructor(user: UserEntity) {
    this.user = user;
  }
}
