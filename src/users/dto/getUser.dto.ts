import { UserEntity } from '../entities/user.entity';
import { IsInt, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';

export class GetUserResponse {
  public user: UserEntity;

  constructor(user: UserEntity) {
    this.user = user;
  }
}

export class GetUsersRequest {
  @IsOptional({
    message: 'Limit is optional',
  })
  @Type(() => Number)
  @IsInt({
    message: 'Limit must be an int',
  })
  public limit?: number;

  @IsOptional({
    message: 'Page is optional',
  })
  @Type(() => Number)
  @IsInt({
    message: 'Page must be an int',
  })
  public page?: number;

  constructor(limit?: number, page?: number) {
    this.limit = limit;
    this.page = page;
  }
}

export class GetUsersResponse {
  public users: UserEntity[];

  public count: number;

  constructor(users: UserEntity[], count: number) {
    this.users = users;
    this.count = count;
  }
}
