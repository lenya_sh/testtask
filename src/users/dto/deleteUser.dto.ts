export class DeleteUserResponse {
  public status: boolean;

  constructor(status: boolean) {
    this.status = status;
  }
}
