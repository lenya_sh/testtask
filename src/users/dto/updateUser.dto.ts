import {
  IsEmail,
  IsOptional,
  IsStrongPassword,
  MaxLength,
} from 'class-validator';
import { UserEntity } from '../entities/user.entity';

export class UpdateUserRequest {
  public userId: number;

  @IsOptional({
    message: 'Email is optional',
  })
  @MaxLength(512)
  @IsEmail()
  public email?: string;

  @IsOptional({
    message: 'Password is optional',
  })
  @IsStrongPassword()
  @MaxLength(128)
  public password?: string;

  constructor(userId: number, email?: string, password?: string) {
    this.userId = userId;
    this.email = email;
    this.password = password;
  }
}

export class UpdateUserResponse {
  public user: UserEntity;

  constructor(user: UserEntity) {
    this.user = user;
  }
}
