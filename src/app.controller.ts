import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller()
export class AppController {
  @Get('/healthcheck')
  index(@Res() res: Response) {
    res.status(HttpStatus.OK).send('ok');
  }
}
