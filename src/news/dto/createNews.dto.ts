import { IsDefined, IsString, MaxLength } from 'class-validator';
import { NewsEntity } from '../entities/news.entity';

export class CreateNewsRequest {
  public ownerId: number;

  @IsDefined({
    message: 'Title is required',
  })
  @IsString()
  @MaxLength(512)
  public title: string;

  @IsDefined({
    message: 'Content is required',
  })
  @IsString()
  public content: string;

  constructor(ownerId: number, title: string, content: string) {
    this.ownerId = ownerId;
    this.title = title;
    this.content = content;
  }
}

export class CreateNewsResponse {
  public headline: NewsEntity;

  constructor(headline: NewsEntity) {
    this.headline = headline;
  }
}
