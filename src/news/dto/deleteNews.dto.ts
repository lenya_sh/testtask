export class DeleteNewsResult {
  public status: boolean;

  constructor(status: boolean) {
    this.status = status;
  }
}
