import { IsOptional, IsString, MaxLength } from 'class-validator';
import { NewsEntity } from '../entities/news.entity';

export class UpdateNewsRequest {
  public ownerId: number;
  public headlineId: number;

  @IsOptional({
    message: 'Title is optional',
  })
  @IsString()
  @MaxLength(512)
  public title?: string;

  @IsOptional({
    message: 'Content is optional',
  })
  @IsString()
  public content?: string;

  constructor(
    ownerId: number,
    headlineId: number,
    title?: string,
    content?: string,
  ) {
    this.ownerId = ownerId;
    this.headlineId = headlineId;
    this.title = title;
    this.content = content;
  }
}

export class UpdateNewsResponse {
  public headline: NewsEntity;

  constructor(headline: NewsEntity) {
    this.headline = headline;
  }
}
