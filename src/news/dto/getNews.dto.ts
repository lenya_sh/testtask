import { IsInt, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';
import { NewsEntity } from '../entities/news.entity';

export class GetHeadlineResponse {
  public headline: NewsEntity;

  constructor(headline: NewsEntity) {
    this.headline = headline;
  }
}

export class GetNewsRequest {
  @IsOptional({
    message: 'Limit is optional',
  })
  @Type(() => Number)
  @IsInt({
    message: 'Limit must be an int',
  })
  public limit?: number;

  @IsOptional({
    message: 'Page is optional',
  })
  @Type(() => Number)
  @IsInt({
    message: 'Page must be an int',
  })
  public page?: number;

  constructor(limit?: number, page?: number) {
    this.limit = limit;
    this.page = page;
  }
}

export class GetNewsResponse {
  public news: NewsEntity[];

  public count: number;

  constructor(news: NewsEntity[], count: number) {
    this.news = news;
    this.count = count;
  }
}
