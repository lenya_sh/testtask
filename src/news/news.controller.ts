import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  Query,
  UseFilters,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { CreateNewsRequest } from './dto/createNews.dto';
import { JWTAuthGuard } from '../auth/guard/jwtAuth.guard';
import { RequestWithUser } from '../auth/types/requestWithUser';
import { GetNewsRequest } from './dto/getNews.dto';
import { UpdateNewsRequest } from './dto/updateNews.dto';
import { MainExceptionFilter } from '../exceptions/main-exception.filter';

@Controller('news')
@UseFilters(MainExceptionFilter)
@UsePipes(ValidationPipe)
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Post()
  @UseGuards(JWTAuthGuard)
  create(
    @Req() requestWithUser: RequestWithUser,
    @Body() body: CreateNewsRequest,
  ) {
    return this.newsService.create(
      new CreateNewsRequest(requestWithUser.user.id, body.title, body.content),
    );
  }

  @Get()
  findAll(@Query() query: GetNewsRequest) {
    return this.newsService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.newsService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(JWTAuthGuard)
  update(
    @Param('id') id: string,
    @Req() requestWithUser: RequestWithUser,
    @Body() body: UpdateNewsRequest,
  ) {
    return this.newsService.update(
      new UpdateNewsRequest(
        requestWithUser.user.id,
        +id,
        body.title,
        body.content,
      ),
    );
  }

  @Delete(':id')
  @UseGuards(JWTAuthGuard)
  remove(@Param('id') id: string, @Req() req: RequestWithUser) {
    return this.newsService.remove(req.user.id, +id);
  }
}
