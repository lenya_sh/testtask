import { Injectable } from '@nestjs/common';
import { CreateNewsRequest, CreateNewsResponse } from './dto/createNews.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NewsEntity } from './entities/news.entity';
import { MainException } from '../exceptions/main.exception';
import {
  GetHeadlineResponse,
  GetNewsRequest,
  GetNewsResponse,
} from './dto/getNews.dto';
import { DeleteNewsResult } from './dto/deleteNews.dto';
import { UpdateNewsRequest, UpdateNewsResponse } from './dto/updateNews.dto';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(NewsEntity)
    private readonly newsRepository: Repository<NewsEntity>,
  ) {}

  async create(request: CreateNewsRequest): Promise<CreateNewsResponse> {
    if (await this.findHeadlineByTitle(request.title))
      throw MainException.conflict('Headline with this title already exist');

    const newHeadline = this.newsRepository.create({
      ownerId: request.ownerId,
      title: request.title,
      content: request.content,
    });

    return new CreateNewsResponse(await this.newsRepository.save(newHeadline));
  }

  async findAll(request: GetNewsRequest): Promise<GetNewsResponse> {
    const page = request.page || 1;
    const limit = request.limit || 10;
    const skip = (page - 1) * limit;

    const [news, count] = await this.newsRepository.findAndCount({
      take: limit,
      skip: skip,
    });

    return new GetNewsResponse(news, count);
  }

  async findOne(id: number) {
    const result = await this.newsRepository.findOne({
      where: {
        id: id,
      },
    });

    if (!result)
      throw MainException.invalidData(`Headline with id ${id} not found`);

    return new GetHeadlineResponse(result);
  }

  private async findHeadlineByTitle(
    title: string,
  ): Promise<NewsEntity | undefined> {
    return await this.newsRepository.findOne({
      where: {
        title: title,
      },
    });
  }

  async update(request: UpdateNewsRequest): Promise<UpdateNewsResponse> {
    const headline = await this.newsRepository.findOne({
      where: {
        id: request.headlineId,
        ownerId: request.ownerId,
      },
    });

    if (request.title) {
      if (await this.findHeadlineByTitle(request.title))
        throw MainException.conflict('Headline with this title already exist');

      headline.title = request.title;
    }

    if (request.content) headline.content = request.content;

    return new UpdateNewsResponse(await this.newsRepository.save(headline));
  }

  async remove(userId: number, id: number): Promise<DeleteNewsResult> {
    const deletedResult = await this.newsRepository.softDelete({
      ownerId: userId,
      id: id,
    });

    if (deletedResult.affected < 1)
      throw MainException.invalidData(
        `Failed to remove headline with id ${id}`,
      );

    return new DeleteNewsResult(true);
  }
}
