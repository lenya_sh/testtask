import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from '../../users/entities/user.entity';

@Entity('news')
export class NewsEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ name: 'title', type: 'varchar', length: 512, unique: true })
  public title!: string;

  @Column({ name: 'content', type: 'varchar' })
  public content!: string;

  @Column({ name: 'owner_id', type: 'int' })
  public ownerId!: number;

  @ManyToOne(() => UserEntity, (user) => user.news)
  @JoinColumn({ name: 'owner_id' })
  public owner!: UserEntity[];

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  public createdAt!: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  public updatedAt!: Date;

  @DeleteDateColumn({ name: 'deleted_at', type: 'timestamp' })
  public deletedAt!: Date;
}
