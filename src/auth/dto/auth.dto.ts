import { IsDefined, IsString, MaxLength } from 'class-validator';

export class AuthRequest {
  @IsDefined({ message: 'email is required' })
  @MaxLength(512)
  @IsString()
  public email: string;

  @IsDefined({ message: 'password is required' })
  @IsString()
  public password: string;

  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}

export class AuthResponse {
  public accessToken: string;

  constructor(accessToken: string) {
    this.accessToken = accessToken;
  }
}
