import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { AuthRequest, AuthResponse } from './dto/auth.dto';
import { MainException } from '../exceptions/main.exception';
import * as bcrypt from 'bcrypt';
import { ExternalPayloadType } from './types/external-payload.type';
import { CreateUserRequest } from '../users/dto/createUser.dto';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async auth(request: AuthRequest): Promise<AuthResponse> {
    const user = await this.usersService.getUserByEmail(request.email);
    if (!user)
      throw MainException.invalidData(
        `User with email ${request.email} not found`,
      );

    if (!(await bcrypt.compare(request.password, user.password))) {
      throw MainException.unauthorized();
    }
    const payload = { sub: user.id, userEmail: user.email };
    return new AuthResponse(
      await this.jwtService.signAsync(payload, {
        secret: this.configService.get('JWT_REFRESH_SECRET'),
        expiresIn: `${this.configService.get('JWT_REFRESH_EXPIRATION')}d`,
      }),
    );
  }

  async register(request: AuthRequest): Promise<AuthResponse> {
    if (!!(await this.usersService.getUserByEmail(request.email)))
      throw MainException.invalidData(
        `User with email ${request.email} already exist`,
      );

    await this.usersService.createUser(
      new CreateUserRequest(request.email, request.password),
    );

    return await this.auth(request);
  }

  async checkTokenAndFindUser(jwt: string) {
    try {
      const decodedToken: ExternalPayloadType = <ExternalPayloadType>(
        this.jwtService.verify(jwt)
      );

      if (!decodedToken || !decodedToken?.userEmail)
        throw MainException.invalidData('Некорректный токен');

      const user = await this.usersService.getUserByEmail(
        decodedToken.userEmail,
      );

      if (!user) throw MainException.invalidData('Пользователь не найден');

      return user;
    } catch {
      throw MainException.forbidden('Нет доступа');
    }
  }
}
