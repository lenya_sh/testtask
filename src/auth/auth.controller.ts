import {
  Controller,
  Post,
  Body,
  UseFilters,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { MainExceptionFilter } from '../exceptions/main-exception.filter';
import { Throttle } from '@nestjs/throttler';
import { AuthRequest } from './dto/auth.dto';

@Controller('auth')
@UseFilters(MainExceptionFilter)
@UsePipes(ValidationPipe)
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post()
  @Throttle({ default: { limit: 5, ttl: 3 } })
  async auth(@Body() body: AuthRequest) {
    return this.authService.auth(body);
  }

  @Post('register')
  @Throttle({ default: { limit: 5, ttl: 3 } })
  async register(@Body() body: AuthRequest) {
    return this.authService.register(body);
  }
}
